package com.hamersoft.passwordmanager;

import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.test.ext.junit.runners.AndroidJUnit4;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class PasswordManagerInstrumentedTest extends AbstractInstrumentedTest {

    @Test
    public void useAppContext() {
        assertEquals("com.hamersoft.passwordmanager", context.getPackageName());
    }

    @Test
    public void PasswordManager_Returns_EmptyString_When_Password_Is_Never_Added() {
        main.getPasswordManager().deletePassword();
        assertEquals("", main.getPasswordManager().getPassword());
    }

    @Test
    public void PasswordManager_Returns_EmptyString_When_Password_Is_Added_By_NullOrEmpty() {
        assertTrue(main.getPasswordManager().savePassword(null));
        assertEquals("", main.getPasswordManager().getPassword());
        assertTrue(main.getPasswordManager().savePassword(""));
        assertEquals("", main.getPasswordManager().getPassword());
    }

    @Test
    public void PasswordManager_Returns_True_When_Added_Password() {
        assertTrue(main.getPasswordManager().savePassword(password));
    }

    @Test
    public void PasswordManager_GetPassword_PlainText_When_Added_Password() {
        assertTrue(main.getPasswordManager().savePassword(password));
        assertEquals(password, main.getPasswordManager().getPassword());
    }

    @Test
    public void PasswordManager_GetPassword_Empty_String_When_Deleted_Password() {
        assertTrue(main.getPasswordManager().savePassword(password));
        assertTrue(main.getPasswordManager().deletePassword());
        assertEquals("", main.getPasswordManager().getPassword());
    }

    @Test
    public void PasswordManager_DeletePassword_Returns_True_When_PassWord_DoesNot_Exist() {
        assertTrue(main.getPasswordManager().deletePassword());
    }
}