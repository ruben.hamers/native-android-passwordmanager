package com.hamersoft.passwordmanager;

import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.test.ext.junit.runners.AndroidJUnit4;

import static org.junit.Assert.*;


/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class SharedPrefsInstrumentedTest extends AbstractInstrumentedTest {

    private final String sharedPrefsKey = "my-sharedpreffs-password-key";
    @Test
    public void useAppContext() {
        assertEquals("com.hamersoft.passwordmanager", context.getPackageName());
    }

    @Test
    public void SharedPreferences_Does_Not_Have_Value_When_Never_Added() {
        assertFalse(main.getSharedPreferencesAccess().hasValue(sharedPrefsKey));
    }

    @Test
    public void SharedPreferences_Does_Not_Have_Value_When_Key_NullOrEmpty() {
        assertFalse(main.getSharedPreferencesAccess().hasValue(null));
        assertFalse(main.getSharedPreferencesAccess().hasValue(""));
    }

    @Test
    public void SharedPreferences_Does_Have_Value_When_Added() {
        main.getSharedPreferencesAccess().put(sharedPrefsKey,password);
        assertTrue(main.getSharedPreferencesAccess().hasValue(sharedPrefsKey));
    }
    @Test
    public void SharedPreferences_Does_Not_Throw_Exception_When_Added_Key_NullOrEmpty() {
        main.getSharedPreferencesAccess().put(null,password);
        main.getSharedPreferencesAccess().put("",password);
        assertTrue(true);
    }

    @Test
    public void SharedPreferences_Does_Not_Have_Value_When_Deleted() {
        main.getSharedPreferencesAccess().put(sharedPrefsKey,password);
        main.getSharedPreferencesAccess().delete(sharedPrefsKey);
        assertFalse(main.getSharedPreferencesAccess().hasValue(sharedPrefsKey));
    }

    @Test
    public void SharedPreferences_Does_Not_Throw_Exception_Delete_By_empty_Key() {
        main.getSharedPreferencesAccess().delete(null);
        main.getSharedPreferencesAccess().delete("");
        assertTrue(true);
    }

    @Test
    public void SharedPreferences_Cannot_Get_Value_When_Never_Added() {
        assertEquals("",main.getSharedPreferencesAccess().get(sharedPrefsKey));
    }

    @Test
    public void SharedPreferences_Does_Not_Throw_Exception_When_Get_By_NullOrEmpty() {
        assertEquals("",main.getSharedPreferencesAccess().get(null));
        assertEquals("",main.getSharedPreferencesAccess().get(""));
        assertTrue(true);
    }

    @Test
    public void SharedPreferences_Cannot_Get_Value_When_Added() {
        main.getSharedPreferencesAccess().put(sharedPrefsKey,password);
        assertEquals(password, main.getSharedPreferencesAccess().get(sharedPrefsKey));
    }

    @Test
    public void SharedPreferences_Cannot_Get_Value_When_Deleted() {
        main.getSharedPreferencesAccess().put(sharedPrefsKey,password);
        main.getSharedPreferencesAccess().delete(sharedPrefsKey);
        assertEquals("",main.getSharedPreferencesAccess().get(sharedPrefsKey));
    }


}