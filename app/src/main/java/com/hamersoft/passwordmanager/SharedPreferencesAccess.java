package com.hamersoft.passwordmanager;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesAccess {

    private String alias;
    private Context context;

    public SharedPreferencesAccess(Context context, String alias) {
        this.context = context;
        this.alias = alias;
    }

    public void put(String key, String value) {
        if (Utils.isNullOrEmpty(key))
            return;
        SharedPreferences.Editor editor = getSharedPreferencesEditor();
        editor.putString(key, value);
        savePreferences(editor);
    }

    public String get(String key) {
        if (!Utils.isNullOrEmpty(key) && hasValue(key))
            return getSharedPreferences().getString(key, null);
        return "";
    }

    public void delete(String key) {
        if (!hasValue(key))
            return;
        SharedPreferences.Editor editor = getSharedPreferencesEditor();
        editor.remove(key);
        savePreferences(editor);
    }

    public boolean hasValue(String key) {
        if (Utils.isNullOrEmpty(key))
            return false;
        SharedPreferences sharedPreferences = getSharedPreferences();
        return sharedPreferences.contains(key);
    }

    private SharedPreferences.Editor getSharedPreferencesEditor() {
        SharedPreferences sharedPreferences = getSharedPreferences();
        return sharedPreferences.edit();
    }

    private SharedPreferences getSharedPreferences() {
        return context.getSharedPreferences(alias, Context.MODE_PRIVATE);
    }

    private void savePreferences(SharedPreferences.Editor editor) {
        editor.commit();
    }

}