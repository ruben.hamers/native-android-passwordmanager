package com.hamersoft.passwordmanager;

class Utils {

    static boolean isNullOrEmpty(String value) {
        return value == null || value.isEmpty();
    }

}
